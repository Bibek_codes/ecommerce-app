import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { Navbar } from './Screens/Cart/Navbar/index'
import { Shop } from './Screens/Cart/Shop/index'
import { Cart } from './Screens/Cart/Cart-item/cart'
import { SignInSignUp } from './Screens/SignInSignUp/index'
import { ShopContextProvider } from './Components/context/shop-context'
import Footer from './Screens/Layout/Footer'
import Details from './Screens/Details/index'
import Checkout from './Screens/Checkout';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
    <div className='App'>
    <ShopContextProvider>
          <Router>
            <Navbar></Navbar>
            <Routes>
              <Route path='/' element={<Shop/>}></Route>
              <Route path='/cart' element={<Cart/>}></Route>
              {/* <Route path='/orders' element={<Order/>}></Route> */}
              <Route path='SignInSignUp' element={<SignInSignUp />}></Route>
              <Route path='Details' element={<Details/>}></Route>
              <Route path='checkout' element={<Checkout/>}></Route>
            </Routes>
            <Footer></Footer>
          </Router>
      </ShopContextProvider>
      <ToastContainer />
    </div>
    </>
  );
}

export default App;