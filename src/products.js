import product1 from "./Assets/apple-iphone-img.png";
import product2 from "./Assets/Adidas-shoes.png";
import product3 from "./Assets/skybags.png";
import product4 from "./Assets/Samsung.png";
import product5 from "./Assets/JBL-headphones.png";
import product6 from "./Assets/Alienware-laptop.png";


export const PRODUCTS = [
    {
        id: 1,
        productName:"IPhone",
        price:999,                                                    
        productImage: product1,
    },
    {
        id: 2,
        productName:"Adidas",
        price:30,                                     
        productImage: product2,
    },
    {
        id: 3,
        productName:"Skybags",
        price:25,                                     
        productImage: product3,
    },
    {
        id: 4,
        productName:"Samsung",
        price:400,                                     
        productImage: product4,
    },
    {
        id: 5,
        productName:"JBL",
        price:25,                                     
        productImage: product5,
    },
    {
        id: 6,
        productName:"Alienware",
        price:1000,                                     
        productImage: product6,
    },
];

