import React from 'react'
import './style.css'

const SuccessModal = ({setBuyClick}) => {
  return (
    <div className='outerModal'>
        <div className='innerModal'>
            <div className="closeButtonModal" onClick={()=>setBuyClick(prev => !prev)}>X</div>
            <h3>Congratulations, Your Items will be delivered shortly !</h3>
        </div>
    </div>
  )
}
export default SuccessModal
