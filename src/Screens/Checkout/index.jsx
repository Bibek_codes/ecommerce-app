import React from 'react'
import { useContext, useState, useEffect } from 'react';
import './style.css'
import { ShopContext } from '../../Components/context/shop-context';
import { PRODUCTS } from '../../products';
import SuccessModal from '../SuccessModal';
import { useNavigate } from 'react-router-dom';
import { CartItem } from '../Cart/Cart-item/cart-item';



const Checkout = () => {
    const navigate = useNavigate();
    const {cartItems} = useContext(ShopContext);
    const [buyClick, setBuyClick] = useState(false);
    function handleBuyNow(){
        setBuyClick(prev => !prev);
        // console.log(buyClick);
        setTimeout(()=> navigate("/"), 3000)
    }
  return (
    <div className='checkout'>
        <h1>Checkout</h1>
        <div className='checkoutDetails'>
            <div className='checkoutDetailsTitle'><h3>Delivery Details</h3></div>
            <div className='checkoutDetailsBody'>
            <p>{localStorage.getItem("name")}</p>
                <p>{localStorage.getItem('email')}</p>
                <p>{localStorage.getItem('phone')}</p>
                <form>
                <input type = "radio" id = "1" name='address'></input>
                <label for = "1"><p>{localStorage.getItem('address1')}</p></label>
                <input type = "radio" id = "2" name='address'></input>
                <label for = "2"><p>{localStorage.getItem('address2')}</p></label>
                </form>
            </div>
        </div>
        <div className='checkoutProducts'>
            <div className='checkoutProductsTitle'><h3>Review Items and delivery</h3></div>
            <div className='checkoutProductsBody'>
            {PRODUCTS.map((product) => {
                    if(cartItems[product.id]){
                        return <CartItem key={product.id} data= {product} checkout></CartItem>;
                    }
            })}
            </div>
        </div>
        <div className='checkoutPayment'>
            <div><h3>Payment Method</h3></div>
            <button onClick={handleBuyNow}>Buy Now</button>
        </div>
        {buyClick && <SuccessModal setBuyClick={setBuyClick} 
        />}
        
    </div>
  )
}

export default Checkout
