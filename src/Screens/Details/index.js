import React, {useState} from 'react'
import './style.css'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify'


const Details = () => {

    const navigate = useNavigate()
  
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
  
    // const handleChange = (e) => {
    //   const { name, value } = e.target
    //   setFormValues({...formValues, [name]:value })
    //   console.log(formValues)
    // }
  
    // const handleSubmit = (e) => {
    //   e.preventDefault();
    //   if(name && email && phone && address){
    //     console.log(name, email, phone, address);
    //     navigate('/checkout')
    //   }
    // };
  
    const handleSubmit = (e) => {
      e.preventDefault();
      const regex = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g
      if (name === "") {
        toast.error("Name Is Required");
      } else if (!regex.test(email) && email === "") {
        toast.error("Email Is Required");
      } else if (phone === "") {
        toast.error("Phone is Required");
      } else if (address1 === "") {
        toast.error("Home address is Required");
      } else if (address2 === "") {
        toast.error("Office address is Required");
      } else {
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
        localStorage.setItem("phone", phone);
        localStorage.setItem("address1", address1);
        localStorage.setItem("address2", address2);
        toast.success("User Saved!");
        navigate('/checkout')
      }
    };
  
    return (
      <>
      <div className="detailWrapper">
        <div className="detailCon">
          <h1>User Details</h1>
          <form>
            <h5 className="detailLabel">Name</h5>
            <input type="text" value={name} onChange={e => setName(e.target.value)}/>
            <h5 className="detailLabel">Email</h5>
            <input type="email" value={email} onChange={e => setEmail(e.target.value)}/>
            <h5 className="detailLabel">Phone</h5>
            <input type="tel" value={phone} pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" onChange={e => setPhone(e.target.value)}/>
            <h5 className="detailLabel">Address(Home)</h5>
            <input type="text" value={address1} onChange={e => setAddress1(e.target.value)}/>
            <h5 className="detailLabel">Address(Office)</h5>
            <input type="text" value={address2} onChange={e => setAddress2(e.target.value)}/>
            <button className="detailBuyButton" onClick={handleSubmit}>Proceed to Checkout</button>
          </form>
        </div>
      </div>
      </> 
    );
  };
  
  export default Details;