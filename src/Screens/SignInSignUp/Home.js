import React, {useEffect} from 'react'
import './style.css'
import { useNavigate } from "react-router-dom";

function Home () {
    const navigate = useNavigate();
    useEffect(()=>{
        const timeout = setTimeout(()=> navigate("/"), 3000);

        return ()=> {
            clearTimeout(timeout);
        }
    },[])
    const logout=()=>{
        localStorage.removeItem("signUp")
        window.location.reload()
    }
    const deleteAccount=()=>{ 
        localStorage.clear()
        window.location.reload()
    }
    
    return(
        <div>
            <h1>Welcome {localStorage.getItem('name')} !</h1>
            <p>U can logout or delete your account!</p>
            <button onClick={logout} className="logout">LogOut</button>
            <button onClick={deleteAccount} className="delete">Delete</button>
        </div>
    );
}
export default Home;
