import React,{useEffect, useRef, useState} from 'react'
import Home from './Home'
import './style.css'

export function SignInSignUp() {
    const name=useRef()
    const email=useRef()
    const password=useRef()
    const [showHome,setShowHome]=useState(false)
    const [show,setShow]=useState(false)
 
     const localSignUp=localStorage.getItem("signUp")
     const localEmail=localStorage.getItem("email")
     const localPassword=localStorage.getItem("password")
     const localName=localStorage.getItem("name")
 
     useEffect(()=>{
     if(localSignUp){
         setShowHome(true)
     }
     if(localEmail){
         setShow(true)
     }
     })
    const handleClick=()=>{
        if(name.current.value&&email.current.value && password.current.value)
       {
         localStorage.setItem("name",name.current.value)
         localStorage.setItem("email",email.current.value)
         localStorage.setItem("password",password.current.value)
         localStorage.setItem("signUp",email.current.value)
         alert("Account created successfully!!")
         window.location.reload()
       }
    }
 
    const handleSignIn=()=>{
     if(email.current.value===localEmail&&password.current.value===localPassword){
         localStorage.setItem("signUp",email.current.value)
         window.location.reload()
     }else{
         alert("Please Enter valid Credential")
     }
    }
     return(
         <div>
       {showHome?<Home/>:
       (show?
         <div className='container'>
         <h2>SignIn</h2>
         <form>
         <label for="email">Email:</label>
         <input type="email" id="email" name="email" ref={email} required></input>
 
         <label for="password">Password:</label>
         <input type="password" id="password" name="password" ref={password} required></input>
 
         <input type="submit" onClick={handleSignIn} value="SignIn"></input>
       </form>
       </div>
       :
       <div className='container'>
       <h2>SignUp</h2>
       <form>
         <label for="name">Name:</label>
         <input type="text" id="name" name="name" ref={name} required></input>
 
         <label for="email">Email:</label>
         <input type="email" id="email" name="email" ref={email} required></input>
 
         <label for="password">Password:</label>
         <input type="password" id="password" name="password" ref={password} required></input>
 
         <input type="submit" onClick={handleClick} value="Register"></input>
       </form>
       </div>)
       }
     </div>
   );
 }

export default SignInSignUp
