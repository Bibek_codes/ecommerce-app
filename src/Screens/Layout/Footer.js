import React from 'react'
import './style.css'

const Footer = () => {
  return (
    <div className="bg-dark text-white text-center p-4 mt-3">
    <h3>Under the guidance of :</h3>
    <h6>Susanta and Soumya Sir</h6>
    <h7>BIPROS_23</h7>
  </div>
  )
}

export default Footer
