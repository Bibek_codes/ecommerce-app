import React from 'react'
import { PRODUCTS } from '../../../products';
import { Product } from '../Product'
import './style.css'
import SearchPage from '../SearchPage';


export const Shop = () => {
    return (
        <div className="shop">
            <div className="shopTitle">
                <h1><b>Super Market</b></h1>
            </div>
            <SearchPage/>
            {/* <div class="search">
                <i class="fa fa-search"></i>
                <input type="text" class="form-control" placeholder="Search any product..."></input>
                <button class="btn btn-primary">Search</button>
            </div> */}
            <div className="products">
                {PRODUCTS.map((product) => (
                    <Product data={product} key={product.id}></Product>
                ))}  
            </div>
        </div>
    );
};

