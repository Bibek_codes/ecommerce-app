import React,{useContext} from 'react'
import { ShopContext } from '../../../Components/context/shop-context';
import'./cart.css'

export const CartItem = (props) => {
    // console.log(props);
    const {id,productName,price,productImage} = props.data;
    const {checkout} = props;
    const {cartItems, addToCart, removeFromCart, updateCartItemCount} = useContext(ShopContext)
    return (
        <div className='cartItem'>
            <img src={productImage}></img>
            <div className='description'>
                <p>
                    <b>{productName}</b>
                </p>
                <p>${price}</p>
                {!checkout && <div className='count-handler'>
                    <div className='but'>
                    <button onClick={() => removeFromCart(id)}> - </button>
                    <input value={cartItems[id]} onChange={(e) => updateCartItemCount(Number(e.target.value),id)}></input>
                    <button onClick={() => addToCart(id)}> + </button>
                    </div>
                </div>}
                {checkout && <p>qty - {cartItems[id]}</p>}
            </div>
        </div>
    );
};