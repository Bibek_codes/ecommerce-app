import React, {useContext} from 'react'
import { PRODUCTS } from '../../../products';
import { ShopContext } from '../../../Components/context/shop-context';
import { CartItem } from './cart-item';
import './cart.css'

import {useNavigate} from "react-router-dom";


export const Cart = () => {
    const { cartItems, getTotalCartAmount } = useContext(ShopContext);
    const totalAmount = getTotalCartAmount()
    
    const navigate = useNavigate()
    return (
        <>
        <div className="cart">
            <div>
                <h1><b>Cart Items</b></h1>
            </div>
            <div className='cartItems'>
                {PRODUCTS.map((product) => {
                    if(cartItems[product.id]){
                        return <CartItem data= {product}></CartItem>;
                    }
                })}
            </div>
            { totalAmount > 0 ? (
            <div className='checkout'>
                <p>Subtotal: ${totalAmount}</p>
                <button className="checkoutButton" onClick={()=>navigate('/details')}>Proceed to checkout</button>
            </div>):( <>
                <h5>Cart is Empty!</h5>
                <button onClick={()=> navigate('/')}>Continue Shopping</button>
            </>
            ) }
            
        </div>
        </>
    );
};