import React, {useEffect,useState} from "react";
import product1 from '../../../Assets/apple-iphone-img.png'
import product2 from '../../../Assets/Adidas-shoes.png'
import product3 from '../../../Assets/skybags.png'
import product4 from '../../../Assets/Samsung.png'
import product5 from '../../../Assets/JBL-headphones.png'
import product6 from '../../../Assets/Alienware-laptop.png'



export function SearchPage(){
    const [searchVal,setSearchVal]=useState([]);
    const [searchText, setSearchText] = useState(""); 

    const values = 
        [
            {
                id: 1,
                productName:"IPhone",
                price:999,                                                    
                productImage: product1,
            },
            {
                id: 2,
                productName:"Adidas",
                price:30,                                     
                productImage: product2,
            },
            {
                id: 3,
                productName:"Skybags",
                price:25,                                     
                productImage: product3,
            },
            {
                id: 4,
                productName:"Samsung",
                price:400,                                     
                productImage: product4,
            },
            {
                id: 5,
                productName:"JBL",
                price:25,                                     
                productImage: product5,
            },
            {
                id: 6,
                productName:"Alienware",
                price:1000,                                     
                productImage: product6,
            },
        ]
    const handleSearch = (e) =>{
        e.preventDefault()
        var lowerCase = searchText.toLowerCase()
        console.log(lowerCase)
        var result = values.filter((item)=>item.productName.toLowerCase().includes(lowerCase))
        setSearchVal(result)
    } 

    const handleChange = (e) =>{
        // if(e.target.value.length == 0){
        //     setSearchVal(values)
        // }
        setSearchText(e.target.value)
    }

    // useEffect(()=>{
    //     setSearchVal(values)
    // },[])
    return(
        <div style={{margin:"60px"}}>
            <form onSubmit={(e)=>handleSearch(e)}>
                <div>
                <input onChange={(e)=>handleChange(e)} name="search" placeholder="Type to search"/><button>Search</button>
                <div style={{width: "100%" }}><br></br>{searchVal.map(item=><h1>{item.productName}</h1>)}</div>
                </div>
        
            </form> 
        </div>
    );
}
export default SearchPage;