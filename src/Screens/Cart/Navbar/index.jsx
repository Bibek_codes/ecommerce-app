import React from 'react'
import {Link} from 'react-router-dom'
import { ShoppingCart } from 'phosphor-react';
import './style.css'

export const Navbar = () => {
  return (
    <div className="navbar">
            <div className="links">
                <Link to="/">Shop</Link>
                <Link to="/cart">
                    <ShoppingCart size={30}></ShoppingCart>
                </Link>
                <Link className="signIn_button" to='/SignInSignUp'>SignIn/SignUp</Link>
            </div>
        </div>
    );
};

